# Arduino Fish Tank Light Controller

## Purpose

The purpose of this sketch is to utilize the ATMEL32u4 microprocessor on the
Arduino Leonard to control keyboard/mouse inputs to a computer via a USB
connection.  This is then taken advantage of in order to simplify monotonous,
repetitive tasks into a simple button press.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/arduino_box_labels.git

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/arduino_box_labels/src/master/LICENSE.txt) file for
details.

