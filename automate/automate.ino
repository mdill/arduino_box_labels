#include <Keyboard.h>

/*
20181022 - Michael Dill

An Arduino sketch designed to simplify repetitive tasks by utilizing the
functionality of the ATMEL32u4's ability to emulate mouse/keyboard input via a
USB connection.

Currently, this sketch is designed to work with the Adafruit Feather Basic Proto.

Three pushbuttons are required, to step this sketch through each function, and
have been properly debounced.  All three pushbuttons are also connected to
individual pull-down resistors, in order to assure constant/predictable
functionality.

One button is used to copy/paste author information into a Microsoft Word
document, then remove any formatting, then return to the browser.

The second button copies/pastes call number information to the same MS Word
document, removes formatting, removes the middle space and replaces it with an
underscore, then returns to the browser.

The third button copies/pastes the barcode to the MS Word document, removes
the carriage return, then returns to the web browser.
*/

// Pins for each pushbutton
const int barcodePin = 6;
const int authorPin = 9;
const int callNumPin = 10;

// Pre-defined debounce delay
unsigned long debounceDelay = 50;

// Various button states
int barcodeLastState = LOW;
int authorLastState = LOW;
int callNumLastState = LOW;
int barcodeButtonState;
int authorButtonState;
int callNumButtonState;
unsigned long barcodeDebounceTime = 0;
unsigned long authorDebounceTime = 0;
unsigned long callNumDebounceTime = 0;

void setup(){
    pinMode( barcodePin, INPUT );
    pinMode( callNumPin, INPUT );
    pinMode( authorPin, INPUT );

    Keyboard.begin();  // Begin keyboard connection to computer
}

// Sway to next open window
void altTab(){
    Keyboard.press( KEY_LEFT_ALT );
    Keyboard.write( KEY_TAB );
    Keyboard.releaseAll();
}

// Paste the contents of the clipboard
void paste(){
    Keyboard.press( KEY_LEFT_CTRL );
    Keyboard.write( '.' );
    Keyboard.releaseAll();
}

// Copy the selected text
void copy(){
    Keyboard.press( KEY_LEFT_CTRL );
    Keyboard.write( 'i' );
    Keyboard.releaseAll();
}

// Remove MSW pasted formatting
void removeFormatting(){
    Keyboard.write( KEY_LEFT_CTRL );
    Keyboard.write( 'k' );
}

// FIRST BUTTON //
// Function to copy and paste the barcode then move to the author box
void pasteBarcode(){
    copy();  // Copy the selected barcode
    altTab();  // Change to MSW document

    paste();  // Paste the barcode
    removeFormatting();  // Remove pasted formatting

    for( int i; i < 2; i++ ){   // Go back to the "Author" column
        Keyboard.press( KEY_LEFT_CTRL );
        Keyboard.write( KEY_LEFT_SHIFT );
        Keyboard.releaseAll();
    }

    altTab();   // Go back to the web page
}

// SECOND BUTTON //
// Function to copy and paste author's name then move to the call number box
void pasteAuthor(){
    copy();  // Copy the selected author's name
    altTab();  // Go to the MSW document

    paste();  // Paste the author's name
    removeFormatting();  // Remove the pasted formatting

    Keyboard.write( KEY_TAB );  // Move to the call number box
    
    altTab();  // Go back to the web page
}

// THIRD BUTTON //
// Function to copy and paste the call number, then go back to the barcode box
void pasteCallNumber(){
    copy();  // Copy the selected call number
    altTab();  // Change to MSW document

    paste();  // Paste the call number
    removeFormatting();  // Remove pasted formatting

    for( int i; i < 2; i++ ){  // Move the cursor to the space in the call num
        Keyboard.press( KEY_LEFT_CTRL );
        Keyboard.write( KEY_LEFT_ARROW );
        Keyboard.releaseAll();
    }

    Keyboard.write( KEY_BACKSPACE );  // Replace space with underscore
    Keyboard.write( '"' );

    Keyboard.write( KEY_TAB );  // Move the cursor to the next barcode box
    Keyboard.write( KEY_DOWN_ARROW );

    altTab();  // Go back to the web page
}

void loop(){
    // Get the state of each button, at the beginning of our loop
    int barcodeCurrentState = digitalRead( barcodePin );
    int authorCurrentState = digitalRead( authorPin );
    int callNumCurrentState = digitalRead( callNumPin );

    // Start the debounce timer for any of the buttons, if a press is received
    if( barcodeCurrentState != barcodeLastState )
        barcodeDebounceTime = millis();
    if( authorCurrentState != authorLastState )
        authorDebounceTime = millis();
    if( callNumCurrentState != callNumLastState )
        callNumDebounceTime = millis();

    // If the timer for the barcode button debouncer is up
    if( (millis() - barcodeDebounceTime) > debounceDelay &&
            barcodeCurrentState != barcodeButtonState ){
        barcodeButtonState = barcodeCurrentState;

        // If we've pushed the barcode button
        if( barcodeButtonState == HIGH )
            pasteBarcode();
    }

    // If the timer for the call number debouncer is up
    if( (millis() - callNumDebounceTime) > debounceDelay &&
            callNumCurrentState != callNumButtonState ){
        callNumButtonState = callNumCurrentState;

        // If we've pushed the call num button
        if( callNumButtonState == HIGH )
            pasteCallNumber();
    }


    // If the timer for the author's name debouncer is up
    if( (millis() - authorDebounceTime) > debounceDelay &&
            authorCurrentState != authorButtonState ){
        authorButtonState = authorCurrentState;

        // If we've pushed the author button
        if( authorButtonState == HIGH )
            pasteAuthor();
    }

    // Move our pushbutton states to storage for the next loop
    barcodeLastState = barcodeCurrentState;
    authorLastState = authorCurrentState;
    callNumLastState = callNumCurrentState;
}

